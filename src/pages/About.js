import React, { Component, Fragment } from "react";
import { Redirect } from "react-router";
import NavigationBar from "../components/NavigationBar";

class About extends Component {
  state = { isAuthenticated: false };

  checkUser = () => {
    const token = localStorage.getItem("token");
    if (!!token) {
      return this.setState({ isAuthenticated: true });
    }
  };

  componentWillMount() {
    this.checkUser();
  }

  render() {
    const { isAuthenticated } = this.state;
    if (!isAuthenticated) return <Redirect to="/login" />;
    return (
      <Fragment>
        <NavigationBar></NavigationBar>
        <h1>yang ini halaman about</h1>
      </Fragment>
    );
  }
}

export default About;
