import React, { Component, Fragment } from "react";
import { Container } from "reactstrap";
import NavigationBar from "../components/NavigationBar";

class Contact extends Component {
  state = { image: null };

  file = React.createRef();

  handleSubmit = (event) => {
    event.preventDefault();
    // upload backend bisa pakai axios
  };

  handleFileChange = (event) => {
    const file = this.file.current.files[0];
    const reader = new FileReader();
    reader.addEventListener(
      "load",
      () => {
        this.setState({ image: reader.result });
      },
      false
    );
    if (file.type.includes("image/")) reader.readAsDataURL(file);
  };
  render() {
    const { image } = this.state;
    return (
      <Fragment>
        <NavigationBar />
        <Container className="p-4">
          {!!image && <img src={image} alt="preview" />}
          <form>
            <input
              type="file"
              name="photo"
              ref={this.file}
              onChange={this.handleFileChange}
            />
            <input type="submit" value="submit" />
          </form>
        </Container>
      </Fragment>
    );
  }
}

export default Contact;
