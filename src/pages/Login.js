import React, { Component } from "react";
import { withRouter } from "react-router";
import { Container } from "reactstrap";

class Response extends String {
  json = () => {
    return JSON.parse(this);
  };
}

function mockFetch(url, { body }) {
  const { email, password } = body;
  if (email !== "admin@admin.com" || password !== "password") {
    return Promise.reject("Email atau password salah!");
  }
  return Promise.resolve(
    new Response(JSON.stringify({ accessToken: "ini token" }))
  );
}

class Login extends Component {
  state = { email: null, password: null };
  set = (name) => (event) => this.setState({ [name]: event.target.value });

  handleChangePassword = event => {
    this.setState({password : event.value})
  }

  handleSubmit = (event) => {
    event.preventDefault();
    const { email, password } = this.state;
    const { history } = this.props;
    mockFetch("http://pura-pura.com/api", { body: { email, password } })
      .then((response) => response.json())
      .then((json) => {
        localStorage.setItem("token", json.accessToken);
        history.push("/");
      })
      .catch((error) => alert(error));
  };

  render() {
    return (
      <Container className="p-4 d-flex flex-column align-items-center justify-content-center">
        <h1>LOGIN</h1>
        <form onSubmit={this.handleSubmit}>
          <div>
            <label>
              Email : 
              <input
                type="email"
                name="email"
                onChange={this.set("email")}
              ></input>
            </label>
          </div>
          <div>
            <label>
              Password :
              <input
                type="password"
                name="password"
                onChange={this.set("password")}
              ></input>
            </label>
          </div>
          <input className="btn btn-primary" type="submit" value="submit" />
        </form>
      </Container>
    );
  }
}

export default withRouter(Login);
