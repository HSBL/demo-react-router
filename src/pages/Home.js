import React, { Component, Fragment } from "react";
import NavigationBar from "../components/NavigationBar";

class Home extends Component {
  render() {
    return (
      <Fragment>
        <NavigationBar></NavigationBar>
        <h1>Halaman Home</h1>
      </Fragment>
    );
  }
}

export default Home;
